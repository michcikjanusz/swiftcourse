//
//  Zad1Tests.swift
//  Zad1Tests
//
//  Created by x193663 on 19/12/2019.
//  Copyright © 2019 x193663. All rights reserved.
//
import Foundation
import XCTest

@testable import Zad1

class Zad1Tests: XCTestCase {
  
  private func sampleTest() {
    
    // MARK: - Given
    
    let target = NewsJSON.everything
    let sampleData = target.sampleData
    
    // MARK: - When
    
    if let articleList = try? JSONDecoder().decode(DataArticles.self, from: sampleData) {
      
    // MARK: - Then
      
      let article = articleList.articles.first
      let totalResults = articleList.totalResults
      XCTAssertEqual(article?.title, """
      Teen SIM-swapper allegedly stole $1M, bought custom jewelry with Bitcoin
      """)
      XCTAssertEqual(article?.description, """
      A 19-year-old SIM-swapper has been charged for allegedly stealing more than $1 million in cryptocurrency. Yousef Selassie reportedly targeted at least 75 victims across the United States in a sophisticated SIM swapping attack. He’s facing several charges, inc…
      """)
      XCTAssertEqual(article?.url, "https://thenextweb.com/hardfork/2019/12/19/sim-swapper-stole-1m-bought-jewelry-bitcoin/")
      XCTAssertEqual(article?.urlToImage, """
      https://img-cdn.tnwcdn.com/image/hardfork?filter_last=1&fit=1280%2C640&url=https%3A%2F%2Fcdn0.tnwcdn.com%2Fwp-content%2Fblogs.dir%2F1%2Ffiles%2F2019%2F12%2FUntitled-design-63.jpg&signature=1462076e33c33ffc6ee8b13547c9b41b
      """)
      XCTAssertEqual(article?.content, """
      A 19-year-old SIM-swapper has been charged for allegedly stealing more than $1 million in cryptocurrency.\r\nYousef Selassie reportedly targeted at least 75 victims across the United States in a sophisticated SIM swapping attack.\r\nHes facing several charges, in… [+1635 chars]
      """)
      XCTAssertEqual(article?.author, "Yessi Bello Perez")
      XCTAssertEqual(article?.source.id, "the-next-web")
      XCTAssertEqual(totalResults, 3805)
    } else {
      XCTFail()
    }
  }
}
