//
//  DataItem+re.swift
//  Zad1
//
//  Created by x193663 on 17/12/2019.
//  Copyright © 2019 x193663. All rights reserved.
//

import Foundation

protocol DataArticleRep {
    func setItem(_ item: DataArticles.DataArticle)
}
