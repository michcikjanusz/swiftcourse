//
//  TableViewCell.swift
//  Zad1
//
//  Created by x193663 on 23/12/2019.
//  Copyright © 2019 x193663. All rights reserved.
//

import UIKit
import Nuke

class TableViewCell: UITableViewCell {
  
// MARK: - IBOUTLETS

  @IBOutlet private weak var descLabel: UILabel!
  @IBOutlet private weak var titleLabel: UILabel!
  @IBOutlet private weak var imageArticle: UIImageView!
}

// MARK: - LIFECYCLE

extension TableViewCell: DataArticleRep {
  func setItem(_ item: DataArticles.DataArticle) {
    guard let img = item.urlToImage,
      let title = item.title,
      let shortDesc = item.description,
      let imgUrl = URL(string: img)
    else {
      return
    }
    titleLabel.text = String(title.split(separator: " ")[0]) + " " + String(title.split(separator: " ")[1]) + "(...)"
    descLabel.text = String(shortDesc.prefix(60)) + "(see more...)"
    Nuke.loadImage(with: imgUrl, into: imageArticle)
  }
}
