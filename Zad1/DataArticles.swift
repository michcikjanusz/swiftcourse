//
//  DataItem.swift
//  Zad1
//
//  Created by x193663 on 17/12/2019.
//  Copyright © 2019 x193663. All rights reserved.
//

import Foundation

struct DataArticles: Decodable {
  
  struct DataSource: Decodable {
    let id: String?
    let name: String?
  }
  struct DataArticle: Decodable {
    let author: String?
    let title: String?
    let description: String?
    let url: String?
    let urlToImage: String?
    let publishedAt: String?
    let content: String?
    let source: DataSource
    
  }
  let articles: [DataArticle]
  let status: String?
  let totalResults: Int?
}
