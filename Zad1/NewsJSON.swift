//
//  Dataitem.swift
//  Zad1
//
//  Created by x193663 on 17/12/2019.
//  Copyright © 2019 x193663. All rights reserved.
//

import Foundation
import Moya

// MARK: - LIFECYCLE

public enum NewsJSON {
  static private let apiKey = "1daced2041b348c69bcdde738577c867"
  
  case everything
  
}
extension NewsJSON {
  
  private func todayDate() -> String {
    let today = Date()
    let monthAgo = Calendar.current.date(byAdding: .day, value: -6, to: today)
    let formatter = DateFormatter()
    formatter.dateFormat = "y/MM/dd"
    let dateAgo = formatter.string(from: monthAgo!)
    return dateAgo
  }
}

extension NewsJSON: TargetType {
  
  public var baseURL: URL {
    let webAddress = "https://newsapi.org/v2"
    guard let url = URL(string: webAddress) else {fatalError("Website is not available")}
    return url
  }
  
  public var path: String {
    switch self {
    case .everything: return "/everything"
    }
  }
    
  public var method: Moya.Method {
    switch self {
    case .everything: return .get
    }
  }
    
  public var sampleData: Data {
    switch self {
    case.everything:
        if let sampleUrl = Bundle.main.url(forResource: "Test", withExtension: "json"),
        let sampleData = try? Data(contentsOf: sampleUrl) {
            return sampleData
        }
    }
    return ErrorHandling.errorMsg
  }
  
  public var task: Task {
    switch self {
    case .everything: return .requestParameters(
        parameters: [
          "q": "bitcoin",
          "from": todayDate(),
          "sortBy": "publishedAt",
          "apikey": NewsJSON.apiKey],
        encoding: URLEncoding.default)
    }
  }
  
  public var headers: [String: String]? {
    return ["Content-Type": "application/json"]
  }
    
  public var validationType: ValidationType {
    return .successCodes
  }
  
}
