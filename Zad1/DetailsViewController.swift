//
//  DetailsViewController.swift
//  Zad1
//
//  Created by x193663 on 14/01/2020.
//  Copyright © 2020 x193663. All rights reserved.
//

import UIKit

class DetailsViewController: UIViewController {
  
// MARK: - VARIABLES
  
  var item: DataArticles.DataArticle?
 
// MARK: - IBOUTLETS
  
  @IBOutlet private weak var detailsTitleLabel: UILabel!
  @IBOutlet private weak var detailsContentLabel: UILabel!
  @IBOutlet private weak var detailsAuthorLabel: UILabel!
  @IBOutlet private weak var detailsSourceLabel: UILabel!
  @IBOutlet private weak var detailsPublishedAt: UILabel!
  
// MARK: - LIFECYCLE
  
  override func viewDidLoad() {
    super.viewDidLoad()

    update()
    navigationItems()
  }
  
  private func navigationItems() {
    let naviShare = UIBarButtonItem(barButtonSystemItem: .action, target: self, action: #selector(onShare(sender:)))
    let naviBrowser = UIBarButtonItem(barButtonSystemItem: .search, target: self, action: #selector(onBrowser(sender:)))
    navigationItem.setRightBarButtonItems([naviBrowser, naviShare], animated: true)
  }
  
  private func update() {
    detailsTitleLabel.text = item?.title
    detailsContentLabel.text = item?.description
    detailsAuthorLabel.text = item?.author
    detailsSourceLabel.text = item?.source.name
    detailsPublishedAt.text = item?.publishedAt
  }

  @objc private func onShare(sender: UIView) {
    let textToShare = "Check out, this is interesting"
    guard let website = item?.url,
    let myWebsite = URL(string: website) else {
      return
    }
    let activityVC = UIActivityViewController(activityItems: [textToShare, myWebsite], applicationActivities: nil)
    activityVC.popoverPresentationController?.sourceView = sender
    self.present(activityVC, animated: true, completion: nil)
  }
  
  @objc private func onBrowser(sender: UIView) {
    guard let website = item?.url,
    let myWebsite = URL(string: website) else {
      return
    }
    UIApplication.shared.open(myWebsite)
  }
}
