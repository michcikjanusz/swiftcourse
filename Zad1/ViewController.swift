//
//  ViewController.swift
//  Zad1
//
//  Created by x193663 on 16/12/2019.
//  Copyright © 2019 x193663. All rights reserved.
//

import UIKit
import Moya

class ViewController: UIViewController {
  
  // MARK: - VARIABLES
  
  let provider = MoyaProvider<NewsJSON>()
  var items: [DataArticles.DataArticle] = [] {
    didSet {
      tableCellPrepare()
    }
  }
  
  // MARK: - IBOUTLETS
  
  @IBOutlet private weak var tableView: UITableView!
  
  // MARK: - LIFECYCLE
  
  override func viewDidLoad() {
    super.viewDidLoad()
    
    setupData()
    setupTable()
  }
  
  private func tableCellPrepare() {
    self.tableView.reloadData()
    self.tableView.tableFooterView = UIView(frame: .zero)
    UIView.animate(withDuration: 3) {
      self.tableView.alpha = 1
    }
  }
  
  private func setupTable() {
    self.title = "LastWeekNews" 
    tableView.dataSource = self
    tableView.reloadData()
  }
  
  private func setupData() {
    provider.request(.everything) { [weak self] result in
      guard let self = self else {
        return
      }
      switch result {
      case .success(let response ):
        do {
          let decoded = try JSONDecoder().decode(DataArticles.self, from: response.data)
          self.items = decoded.articles
        } catch {
          debugPrint(error)
        }
      case .failure:
        self.connectionInterupt()
      }
    }
  }
  
  private func connectionInterupt() {
    let popUpMsg = UIAlertController(title: "No connection", message: "Please check connection and try again later.", preferredStyle: .alert)
    let confirm = UIAlertAction(title: "OK", style: .default, handler: nil)
    popUpMsg.addAction(confirm)
    present(popUpMsg, animated: true, completion: nil)
  }
  
  override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
    super.prepare(for: segue, sender: sender)
    
    guard let cell = sender as? UITableViewCell,
      let indexPath = self.tableView.indexPath(for: cell),
      let destination = segue.destination as? DetailsViewController else {
        return
      }
    let articlesItems = self.items[indexPath.row]
    destination.item = articlesItems
  }
}

extension ViewController: UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
      return items.count
    }
  
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let item = items[indexPath.row]
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath )
        if let cell = cell as? DataArticleRep {
          cell.setItem(item)
        }
      return cell
    }
  }
    
