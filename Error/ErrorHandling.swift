//
//  ErrorHandling.swift
//  Zad1
//
//  Created by x193663 on 20/12/2019.
//  Copyright © 2019 x193663. All rights reserved.
//

import Foundation

enum ErrorHandling: Error {
  
  static let errorMsg: Data! = "Lack of test data".data(using: .utf8)
  
}
